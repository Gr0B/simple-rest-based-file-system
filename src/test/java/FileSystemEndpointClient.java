import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * Created by rgubin on 23.11.15.
 */
@Path("/{path:(.+)?}")
public interface FileSystemEndpointClient {
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    Response getFile(@PathParam("path") String path);

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    Response removeFile(@PathParam("path") String path);

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    Response postFile(@PathParam("path") String path, MultipartFormDataInput input);

    @PUT
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces(MediaType.APPLICATION_JSON)
    Response putFile(@PathParam("path") String path, MultipartFormDataInput input);
}
