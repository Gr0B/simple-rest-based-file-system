import com.rgubin.test.core.FileService;
import org.apache.commons.io.FileUtils;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import java.io.File;
import java.io.IOException;
import java.util.*;

/**
 * Created by rgubin on 23.11.15.
 */
public class TestUtils {
    public static File createTempDir() {
        String temp = System.getProperty("java.io.tmpdir")+ File.separator + "tmp" + System.nanoTime();
        File rootFolder = new File(temp);
        if(!rootFolder.exists())
            rootFolder.mkdir();
        return rootFolder;
    }

    public static FileSystemEndpointClient getClient(String url) {
        ResteasyClient client = new ResteasyClientBuilder().build();
        ResteasyWebTarget target = client.target(url);
        return target.proxy(FileSystemEndpointClient.class);
    }

    public static Map<String, Object> buildMap(File root, File[] content) {
        Map<String, Object> m = FileService.toMap(root);
        m.put("content", FileService.toMap(content));
        return m;
    }

}
