import com.rgubin.test.App;
import com.rgubin.test.core.Config;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.HttpClientBuilder;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.File;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;


/**
 * Created by rgubin on 21.11.15.
 */
@RunWith(JUnit4.class)
public class FileSystemEndpointTest {
    private static File rootFolder;
    private static final String URL = "http://localhost:8080/";

    @BeforeClass
    public static void init() throws IOException {
        rootFolder = TestUtils.createTempDir();
        Config.rootDirectoryPath = rootFolder.getAbsolutePath();
        App.startServer(rootFolder.getAbsolutePath());
    }

    @Test
    public void postFile() throws IOException {
        FileUtils.cleanDirectory(rootFolder);
        String fileName1 = "1";
        String fileName2 = "2";
        File f1 = new File(rootFolder + File.separator + fileName1);
        File f2 = new File(rootFolder + File.separator + fileName2);
        FileUtils.touch(f1);
        HttpEntity entity = MultipartEntityBuilder.create()
                .addBinaryBody("data", f1, ContentType.create("application/octet-stream"), fileName2).build();
        HttpPost request = new HttpPost(URL);
        request.setEntity(entity);
        HttpClientBuilder client = HttpClientBuilder.create();
        client.build().execute(request);
        Map<String, Object> e = TestUtils.buildMap(rootFolder, new File[]{f1,f2});
        Map<String, Object> a = TestUtils.getClient(URL).getFile("/").readEntity(LinkedHashMap.class);
        Assert.assertEquals(e, a);
    }

    @Test
    public void removeFile() throws IOException {
        FileUtils.cleanDirectory(rootFolder);
        String fileName = "1";
        File content = new File(rootFolder + File.separator + fileName);
        FileUtils.touch(content);
        TestUtils.getClient(URL).removeFile(fileName);
        Map<String, Object> e = TestUtils.buildMap(rootFolder, new File[]{});
        Map<String, Object> a = TestUtils.getClient(URL).getFile("/").readEntity(LinkedHashMap.class);
        Assert.assertEquals(e, a);
    }


}
