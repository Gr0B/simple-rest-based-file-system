package com.rgubin.test;


import com.rgubin.test.domain.DomainService;
import com.rgubin.test.domain.Message;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jboss.resteasy.annotations.providers.multipart.MultipartForm;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;


@Path("/{path:(.+)?}")
public class FileSystemEndpoint {

    private static final Logger logger = Logger.getLogger(FileSystemEndpoint.class.getName());
    private static final DomainService domainService = new DomainService();

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getFile(@PathParam("path") String path) {
        logger.log(Level.INFO, "Try to get file or folder with path: " + path);
        if (StringUtils.equals(path, "favicon.ico")) {
            return Response.status(Response.Status.OK).build();
        }
        return Response.status(Response.Status.OK).entity(domainService.get(path)).build();
    }

    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response removeFile(@PathParam("path") String path) {
        logger.log(Level.INFO, "Try to remove file or folder.");
        if (StringUtils.isEmpty(path)) {
            return Response.status(Response.Status.OK).entity(new Message("Can not remove root folder.")).build();
        }
        domainService.remove(path);
        return Response.status(Response.Status.OK).entity(new Message("File at " + path + " was removed.")).build();
    }

    @POST
    @Consumes("multipart/form-data")
    @Produces(MediaType.APPLICATION_JSON)
    public Response postFile(@PathParam("path") String path, @MultipartForm MultipartFormDataInput input) {
        logger.log(Level.INFO, "Try to create file.");
        domainService.create(path, input);
        return Response.status(Response.Status.ACCEPTED).entity(new Message("File was created.")).build();
    }

    @PUT
    @Consumes("multipart/form-data")
    @Produces(MediaType.APPLICATION_JSON)
    public Response putFile(@PathParam("path") String path, @MultipartForm MultipartFormDataInput input) {
        logger.log(Level.INFO, "Try to update file.");
        domainService.update(path, input);
        return Response.status(Response.Status.ACCEPTED).entity(new Message("File was updated.")).build();
    }
}