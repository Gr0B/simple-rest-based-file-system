package com.rgubin.test.domain;

import javax.ws.rs.core.Response;

/**
 * Created by rgubin on 19.11.2015.
 */
public class Message {
    private final String message;

    public Message(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
