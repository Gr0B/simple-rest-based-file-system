package com.rgubin.test.domain.exception;

import java.io.Serializable;

/**
 * Created by rgubin on 22.11.15.
 */
public class ApplicationException extends RuntimeException implements Serializable {

    private static final long serialVersionUID = 1L;

    public ApplicationException(String message, Throwable cause) {
        super(message, cause);
    }

    public ApplicationException(String message) {
        super(message);
    }
}
