package com.rgubin.test.domain.exception;

import com.rgubin.test.domain.Message;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Created by rgubin on 22.11.15.
 */
@Provider
public class ApplicationExceptionMapper implements ExceptionMapper<ApplicationException> {
    @Override
    public Response toResponse(ApplicationException e) {
        return Response.status(Response.Status.CONFLICT).entity(new Message(e.getMessage())).build();
    }
}
