package com.rgubin.test.domain;

import com.rgubin.test.core.FileService;
import com.rgubin.test.domain.exception.ApplicationException;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import java.io.File;
import java.io.IOException;
import java.util.Map;
/**
 * Created by rgubin on 19.11.15.
 */
public class DomainService {
    private static final Logger logger = Logger.getLogger(DomainService.class.getName());

    public Map<String, Object> get(String path) {
        File file = FileService.toFile(path);
        logger.debug("Try to validate file existence: " + file.getAbsolutePath());
        if (!file.exists()) throw new ApplicationException("File not found.");
        return FileService.toMap(file);
    }

    public void remove(String path) {
        File file = FileService.toFile(path);
        try {
            if (file.isDirectory()) {
                FileUtils.deleteDirectory(file);
            } else {
                file.delete();
            }
        } catch (IOException e) {
            throw new ApplicationException("Can not remove file.");
        }
    }

    public void create(String path, MultipartFormDataInput input) {
        try {
            FileService.uploadFile(path, input, FileService.NOT_OVERWRITE);
        } catch (IOException e) {
            throw new ApplicationException("Error while file creation.", e);
        }
    }

    public void update(String path, MultipartFormDataInput input) {
        try {
            FileService.uploadFile(path, input, FileService.OVERWRITE);
        } catch (IOException e) {
            throw new ApplicationException("Error while file updating.", e);
        }
    }

}



