package com.rgubin.test.core;


import com.rgubin.test.domain.exception.ApplicationException;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

import javax.ws.rs.core.MultivaluedMap;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * Created by rgubin on 19.11.2015.
 */
public class FileService {
    private static final Logger logger = Logger.getLogger(FileService.class.getName());
    public static final boolean OVERWRITE = true;
    public static final boolean NOT_OVERWRITE = false;

    public static File toFile(String path) {
        StringUtils.trimToEmpty(path);
        final List<String> splitted = Arrays.asList(StringUtils.splitPreserveAllTokens(path, "/"));
        return new File(Config.rootDirectoryPath + File.separator + StringUtils.join(splitted, File.separator));
    }

    public static File toFile(String path, String name) {
        return toFile(path + File.separator + name);
    }

    private static Object determineContent(File file) {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            Arrays.sort(files);
            return FileService.toMap(files);
        } else {
            try {
                boolean isBase64 = org.apache.commons.net.util.Base64.isArrayByteBase64(IOUtils.toByteArray(file.toURI()));
                return isBase64 ? "base64-encoded-content" : "unknown";
            } catch (IOException e) {
                logger.log(Level.ERROR, e);
                return "unknown";
            }
        }
    }

    public static Map<String, Object> toMap(File file) {
        Map<String, Object> o = new LinkedHashMap<>();
        o.put("name", file.getName());
        o.put("parentPath", file.getParent());
        o.put("isFolder", String.valueOf(file.isDirectory()));
        String size = String.valueOf(FileUtils.sizeOf(file));
        o.put("size", size);
        o.put("content", determineContent(file));
        return o;
    }

    public static List<Map<String, Object>> toMap(File[] files) {
        List<Map<String, Object>> l = new LinkedList<Map<String, Object>>();
        for (File file : files) {
            l.add(toMap(file));
        }
        return l;
    }


    private static void checkUploadFile(File file, boolean overwrite) {
        if (file.exists() && !overwrite) {
            throw new ApplicationException("Sorry, can not create file. This file already exists. Please, use PUT to update it.");
        } else if (!file.exists() && overwrite) {
            throw new ApplicationException("Sorry, this file not exists, so please use POST method to create it.");
        }
    }

    private static byte[] encode(InputStream is) throws IOException {
        byte[] raw = IOUtils.toByteArray(is);
        return Base64.encodeBase64(raw, true);
    }

    public static boolean uploadFile(String path, MultipartFormDataInput input, boolean overwrite) throws IOException {
        Map<String, List<InputPart>> uploadForm = input.getFormDataMap();
        List<InputPart> inputParts = uploadForm.get("data");
        logger.info("Uploading " + inputParts.size() + " input parts.");
        for (InputPart inputPart : inputParts) {
            MultivaluedMap<String, String> header = inputPart.getHeaders();
            String fileName = FileService.getFileName(header);
            File file = FileService.toFile(path, fileName);
            logger.info("Processing part of " + fileName + "file.");
            checkUploadFile(file, overwrite);
            InputStream inputStream = inputPart.getBody(InputStream.class, null);
            FileUtils.writeByteArrayToFile(file, encode(inputStream));
        }
        return true;
    }

    public static String getFileName(MultivaluedMap<String, String> header) {
        String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
        for (String filename : contentDisposition) {
            if ((filename.trim().startsWith("filename"))) {
                String[] name = filename.split("=");
                return name[1].trim().replaceAll("\"", "");
            }
        }
        return "unknown";
    }

}

