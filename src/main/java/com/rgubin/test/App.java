package com.rgubin.test;

import com.rgubin.test.core.Config;
import com.rgubin.test.domain.exception.ApplicationExceptionMapper;
import io.undertow.Undertow;
import org.apache.log4j.Logger;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.jboss.resteasy.plugins.providers.multipart.MimeMultipartProvider;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInputImpl;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataReader;
import org.jboss.resteasy.plugins.server.undertow.UndertowJaxrsServer;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

public class App {
    private static final Logger logger = Logger.getLogger(App.class.getName());

    static class RestApp extends Application {
        @Override
        public Set<Class<?>> getClasses() {
            final Set<Class<?>> clazzes = new HashSet<Class<?>>();
            clazzes.add(FileSystemEndpoint.class);
            clazzes.add(ApplicationExceptionMapper.class);
            clazzes.add(JacksonJaxbJsonProvider.class);
            clazzes.add(MultipartFormDataReader.class);
            return clazzes;
        }

    }

    public static void main(final String[] args) {
        startServer(args[0]);
    }

    public static void startServer(String rootPath) {
        Config.rootDirectoryPath = rootPath;
        UndertowJaxrsServer server = new UndertowJaxrsServer();
        Undertow.Builder b = Undertow.builder().addHttpListener(8080, "localhost");
        server.deploy(new RestApp());
        server.start(b);
        logger.info("Server started with root path: " + Config.rootDirectoryPath);
    }
}
