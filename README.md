prerequisites:
maven 2
jdk 7

installation:

1) go to the source folder

2) execute command:
    mvn clean install

3) go to target directory

   execute command:
    java -jar target/test-1.0-SNAPOT.jar $path-to-root-folder
   example: java -jar target/test-1.0-SNAPOT.jar /home/rgubin/test/

usage:

    upload file:
        curl -i -X POST -H "Content-Type: multipart/form-data" -F "data=@$localFile" http://localhost:8080/

    upload and replace if exists:
        curl -i -X PUT -H "Content-Type: multipart/form-data"  -F "data=@localFile" http://localhost:8080/

    get list of files:
        curl -i -X GET http://localhost:8080/

    remove file:
        curl -i -X DELETE http://localhost:8080/$file

where $file == path to file from root folder
and
$localFile == path to file on client machine